from .app import app
from flask import render_template, redirect, request
from .models import *
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, SubmitField, PasswordField
from wtforms.validators import DataRequired
from hashlib import sha256
from flask_login import login_user, current_user

class LoginForm(FlaskForm):
    username = StringField("Username")
    password = PasswordField("Password")
    def get_authenticated_user(self):
        user = User.query.get(self.username.data)
        if user is None:
            return None
        m = sha256()
        m.update(self.password.data.encode())
        passwd = m.hexdigest()
        return user if passwd == user.password else None


from .loaddb import *
@app.route("/aides/<int:id>")
def aides(id):
    global list_aide
    a = get_aide_by_id(id)
    return render_template("aides_template.html", aide=a)
